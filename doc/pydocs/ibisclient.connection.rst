ibisclient.connection module
============================

Classes:

@module.toc@

.. automodule:: ibisclient.connection
    :members: createConnection, createLocalConnection, createTestConnection
