/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Class representing the top-level container for all XML and JSON results.
 * This may be just a simple textual value or it may contain more complex
 * entities such as people, institutions, groups, attributes, etc.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
@XmlRootElement(name = "result")
public class IbisResult
{
    /** The web service API version number. */
    @XmlAttribute public String version;

    /** The value returned by methods that return a simple textual value. */
    @XmlElement public String value;

    /**
     * The person returned by methods that return a single person.
     * <p>
     * Note that methods that may return multiple people will always use
     * the {@link #people} field, even if only one person was returned.
     */
    @XmlElement public IbisPerson person;

    /**
     * The institution returned by methods that return a single institution.
     * <p>
     * Note that methods that may return multiple institutions will always
     * use the {@link #institutions} field, even if only one institution
     * was returned.
     */
    @XmlElement public IbisInstitution institution;

    /**
     * The group returned by methods that return a single group.
     * <p>
     * Note that methods that may return multiple groups will always use
     * the {@link #groups} field, even if only one group was returned.
     */
    @XmlElement public IbisGroup group;

    /** The identifier returned by methods that return a single identifier. */
    @XmlElement public IbisIdentifier identifier;

    /**
     * The person or institution attribute returned by methods that return
     * a single attribute.
     */
    @XmlElement public IbisAttribute attribute;

    /** If the method failed, details of the error. */
    @XmlElement public IbisError error;

    /**
     * The list of people returned by methods that may return multiple
     * people. This may be empty, or contain one or more people.
     */
    @XmlElementWrapper
    @XmlElement(name = "person")
    public List<IbisPerson> people;

    /**
     * The list of institutions returned by methods that may return multiple
     * institutions. This may be empty, or contain one or more institutions.
     */
    @XmlElementWrapper
    @XmlElement(name = "institution")
    public List<IbisInstitution> institutions;

    /**
     * The list of groups returned by methods that may return multiple
     * groups. This may be empty, or contain one or more groups.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> groups;

    /**
     * The list of attributes returned by methods that return lists of
     * person/institution attributes.
     */
    @XmlElementWrapper
    @XmlElement(name = "attribute")
    public List<IbisAttribute> attributes;

    /**
     * The list of attribute schemes returned by methods that return lists
     * of person/institution attribute schemes.
     */
    @XmlElementWrapper
    @XmlElement(name = "attributeScheme")
    public List<IbisAttributeScheme> attributeSchemes;

    /**
     * In the flattened XML/JSON representation, all the unique entities
     * returned by the method.
     * <p>
     * NOTE: This will be {@code null} unless the "flatten" parameter is
     * {@code true}.
     */
    @XmlElement public Entities entities;

    /** No-arg constructor required by JAXB */
    public IbisResult() { }

    /** More useful constructor, for purely textual results */
    public IbisResult(String value) { this.value = value; }

    @Override
    public String toString() { return value; }

    /**
     * Nested class to hold the full details of all the entities returned
     * in a result. This is used only in the flattened result representation,
     * where each of these entities will have a unique textual ID, and be
     * referred to from the top-level objects returned (and by each other).
     * <p>
     * In the hierarchical representation, this is not used, since all
     * entities returned will be at the top-level, or directly contained in
     * those top-level entities.
     */
    public static class Entities
    {
        /**
         * A list of all the unique people returned by the method. This may
         * include additional people returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "person")
        public List<IbisPerson> people;

        /**
         * A list of all the unique institutions returned by the method.
         * This may include additional institutions returned as a result
         * of the <code>fetch</code> parameter, so this list may contain
         * more entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "institution")
        public List<IbisInstitution> institutions;

        /**
         * A list of all the unique groups returned by the method. This may
         * include additional groups returned as a result of the
         * <code>fetch</code> parameter, so this list may contain more
         * entries than the corresponding field on the enclosing class.
         */
        @XmlElementWrapper
        @XmlElement(name = "group")
        public List<IbisGroup> groups;

        /** Add a person to the list of people, creating it if necessary */
        public void addPerson(IbisPerson person)
        {
            if (people == null) people = new ArrayList<IbisPerson>();
            person.id = person.id();
            people.add(person);
        }

        /** Add an institution to the list of institutions, creating it if necessary */
        public void addInstitution(IbisInstitution institution)
        {
            if (institutions == null) institutions = new ArrayList<IbisInstitution>();
            institution.id = institution.id();
            institutions.add(institution);
        }

        /** Add a group to the list of groups, creating it if necessary */
        public void addGroup(IbisGroup group)
        {
            if (groups == null) groups = new ArrayList<IbisGroup>();
            group.id = group.id();
            groups.add(group);
        }
    }

    /* == Code to help clients unflatten an IbisResult object == */

    /**
     * Nested class to assist during the unflattening process, maintaining
     * efficient maps from IDs to entities (people, institutions and groups).
     */
    @XmlTransient
    protected static class EntityMap
    {
        private Map<String, IbisPerson>         peopleById;
        private Map<String, IbisInstitution>    instsById;
        private Map<String, IbisGroup>          groupsById;

        /** Construct an entity map from a flattened IbisResult. */
        private EntityMap(IbisResult result)
        {
            peopleById = new HashMap<String, IbisPerson>();
            instsById  = new HashMap<String, IbisInstitution>();
            groupsById = new HashMap<String, IbisGroup>();

            if (result.entities.people != null)
                for (IbisPerson person : result.entities.people)
                    peopleById.put(person.id, person);
            if (result.entities.institutions != null)
                for (IbisInstitution inst : result.entities.institutions)
                    instsById.put(inst.id, inst);
            if (result.entities.groups != null)
                for (IbisGroup group : result.entities.groups)
                    groupsById.put(group.id, group);
        }

        /** Get a person from the entity map, given their ID */
        protected IbisPerson getPerson(String id) { return peopleById.get(id); }

        /** Get an institution from the entity map, given its ID */
        protected IbisInstitution getInstitution(String id) { return instsById.get(id); }

        /** Get a group from the entity map, given its ID */
        protected IbisGroup getGroup(String id) { return groupsById.get(id); }
    }

    /**
     * Unflatten this IbisResult object, resolving any internal ID refs
     * to build a fully fledged object tree.
     * <p>
     * This is necessary if the IbisResult was constructed from XML/JSON in
     * its flattened representation (with the "flatten" parameter set to
     * {@code true}).
     * <p>
     * On entry, the IbisResult object may have people, institutions or
     * groups in it with "ref" fields referring to objects held in the
     * "entities" lists. After unflattening, all such references will have
     * been replaced by actual object references, giving an object tree that
     * can be traversed normally.
     *
     * @return This IbisResult object, with its internals unflattened.
     */
    public IbisResult unflatten()
    {
        if (entities != null)
        {
            EntityMap em = new EntityMap(this);

            if (person != null)
                person = person.unflatten(em);
            if (institution != null)
                institution = institution.unflatten(em);
            if (group != null)
                group = group.unflatten(em);

            IbisPerson.unflatten(em, people);
            IbisInstitution.unflatten(em, institutions);
            IbisGroup.unflatten(em, groups);
        }
        return this;
    }
}
