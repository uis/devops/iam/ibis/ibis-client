#!/bin/sh
# Run tox for linting and unit testing.
#
# Any command line arguments to this script are passed to tox.

UID_GID="$(id -u):$(id -g)" TEST_ARGS="${@}" docker-compose --project-name ibis-client build test
UID_GID="$(id -u):$(id -g)" TEST_ARGS="${@}" docker-compose --project-name ibis-client run ${TEST_COMPOSE_EXTRA_ARGS} test
