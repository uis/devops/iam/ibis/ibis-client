/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.ibis.client.ClientConnection;
import uk.ac.cam.ucs.ibis.client.ClientConnection.Method;
import uk.ac.cam.ucs.ibis.client.IbisException;
import uk.ac.cam.ucs.ibis.dto.*;

/**
 * Common methods for searching for objects in the Lookup/Ibis database.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class IbisMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new IbisMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public IbisMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Get the ID of the last (most recent) transaction.
     * <p>
     * A transaction represents an edit made to data in Lookup. Each
     * transaction is assigned a unique, sequential, numeric ID. Thus
     * this last transaction ID will increase each time some data in
     * Lookup is changed.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/last-transaction ]</code>
     *
     * @return The ID of the latest transaction.
     */
    public long getLastTransactionId()
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/last-transaction",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return Long.parseLong(result.value);
    }

    /**
     * Get the current API version number.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/version ]</code>
     *
     * @return The API version number string.
     */
    public String getVersion()
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/version",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.value;
    }
}
