/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.ibis.dto.IbisResult.EntityMap;

/**
 * Class representing an institution contact row, for use by the web
 * services API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class IbisContactRow
{
    /** The contact row's text. */
    @XmlElement public String description;

    /**
     * Flag indicating if the contact row's text is normally displayed in
     * bold.
     */
    @XmlAttribute public boolean bold;

    /**
     * Flag indicating if the contact row's text is normally displayed in
     * italics.
     */
    @XmlAttribute public boolean italic;

    /**
     * A list of the contact row's addresses. This will always be non-null,
     * but may be an empty list.
     */
    @XmlElementWrapper
    @XmlElement(name = "address")
    public List<String> addresses;

    /**
     * A list of the contact row's email addresses. This will always be
     * non-null, but may be an empty list.
     */
    @XmlElementWrapper
    @XmlElement(name = "email")
    public List<String> emails;

    /**
     * A list of the people referred to by the contact row. This will always
     * be non-null, but may be an empty list.
     */
    @XmlElementWrapper
    @XmlElement(name = "person")
    public List<IbisPerson> people;

    /**
     * A list of the contact row's phone numbers. This will always be
     * non-null, but may be an empty list.
     */
    @XmlElementWrapper
    @XmlElement(name = "phoneNumber")
    public List<IbisContactPhoneNumber> phoneNumbers;

    /**
     * A list of the contact row's web pages. This will always be non-null,
     * but may be an empty list.
     */
    @XmlElementWrapper
    @XmlElement(name = "webPage")
    public List<IbisContactWebPage> webPages;

    /* == Code to help clients unflatten an IbisResult object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single IbisContactRow. */
    protected IbisContactRow unflatten(EntityMap   em)
    {
        if (!unflattened)
        {
            unflattened = true;
            IbisPerson.unflatten(em, people);
        }
        return this;
    }

    /** Unflatten a list of IbisContactRow objects (done in place). */
    protected static void unflatten(EntityMap               em,
                                    List<IbisContactRow>    contactRows)
    {
        if (contactRows != null)
            for (int i=0; i<contactRows.size(); i++)
                contactRows.set(i, contactRows.get(i).unflatten(em));

    }
}
