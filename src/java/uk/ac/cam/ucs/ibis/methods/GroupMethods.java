/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.ibis.client.ClientConnection;
import uk.ac.cam.ucs.ibis.client.ClientConnection.Method;
import uk.ac.cam.ucs.ibis.client.IbisException;
import uk.ac.cam.ucs.ibis.dto.*;

/**
 * Methods for querying and manipulating groups.
 *
 * <h4>The fetch parameter for groups</h4>
 * <p>
 * All methods that return groups also accept an optional <code>fetch</code>
 * parameter that may be used to request additional information about the
 * groups returned. For more details about the general rules that apply to
 * the <code>fetch</code> parameter, refer to the {@link PersonMethods}
 * documentation.
 * <p>
 * For groups the <code>fetch</code> parameter may be used to fetch references
 * to people, institutions or other groups. In each case, only non-cancelled
 * people, institutions and groups will be included when fetching references.
 * The following references are supported:
 * <ul>
 * <li>{@code "all_members"} - fetches all the people who are members of the
 * group, including members of groups included by the group, and groups
 * included by those groups, and so on.</li>
 * <li>{@code "direct_members"} - fetches all the people who are direct
 * members of the group, not taking into account any included groups.</li>
 * <li>{@code "members_of_inst"} - if the group is a membership group for an
 * institution, this fetches that institution.</li>
 * <li>{@code "owning_insts"} - fetches all the institutions to which the
 * group belongs.</li>
 * <li>{@code "manages_insts"} - fetches all the institutions that the group
 * manages. Typically this only applies to "Editor" groups.</li>
 * <li>{@code "manages_groups"} - fetches all the groups that this group
 * manages. Note that some groups are self-managed, so this may be a
 * self-reference.</li>
 * <li>{@code "managed_by_groups"} - fetches all the groups that manage this
 * group.</li>
 * <li>{@code "reads_groups"} - fetches all the groups that this group has
 * privileged access to. This means that members of this group can see the
 * members of the referenced groups regardless of the membership visibility
 * settings.</li>
 * <li>{@code "read_by_groups"} - fetches all the groups that have privileged
 * access to this group.</li>
 * <li>{@code "includes_groups"} - fetches all the groups included by this
 * group.</li>
 * <li>{@code "included_by_groups"} - fetches all the groups that include
 * this group.</li>
 * </ul>
 * <p>
 * As with person <code>fetch</code> parameters, the references may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced people, institutions or groups. For example
 * {@code "all_members.email"} will fetch the email addresses of all members
 * of the group. For more information about what can be fetched from
 * referenced people and institutions, refer to the documentation for
 * {@link PersonMethods} and {@link InstitutionMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class GroupMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new GroupMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public GroupMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Return a list of all groups.
     * <p>
     * By default, only a few basic details about each group are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/all-groups ]</code>
     *
     * @param includeCancelled [optional] Whether or not to include cancelled
     * groups. By default, only live groups are returned.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The requested groups (in groupid order).
     */
    public java.util.List<IbisGroup> allGroups(boolean  includeCancelled,
                                               String   fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "includeCancelled", includeCancelled,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/all-groups",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.groups;
    }

    /**
     * Get the groups with the specified IDs or names.
     * <p>
     * By default, only a few basic details about each group are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * The results are sorted by groupid.
     * <p>
     * NOTE: The URL path length is limited to around 8000 characters,
     * which limits the number of groups that this method can fetch. Group
     * IDs are currently 6 characters long, and must be comma separated and
     * URL encoded, which limits this method to around 800 groups by ID,
     * but probably fewer by name, depending on the group name lengths.
     * <p>
     * NOTE: The groups returned may include cancelled groups. It is the
     * caller's responsibility to check their cancelled flags.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/list?groupids=... ]</code>
     *
     * @param groupids [required] A comma-separated list of group IDs or
     * group names (may be a mix of both).
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The requested groups (in groupid order).
     */
    public java.util.List<IbisGroup> listGroups(String  groupids,
                                                String  fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "groupids", groupids,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/list",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.groups;
    }

    /**
     * Find all groups modified between the specified pair of transactions.
     * <p>
     * The transaction IDs specified should be the IDs from two different
     * requests for the last (most recent) transaction ID, made at different
     * times, that returned different values, indicating that some Lookup
     * data was modified in the period between the two requests. This method
     * then determines which (if any) groups were affected.
     * <p>
     * By default, only a few basic details about each group are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * NOTE: All data returned reflects the latest available data about each
     * group. It is not possible to query for old data, or more detailed
     * information about the specific changes made.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/modified-groups?minTxId=...&maxTxId=... ]</code>
     *
     * @param minTxId [required] Include modifications made in transactions
     * after (but not including) this one.
     * @param maxTxId [required] Include modifications made in transactions
     * up to and including this one.
     * @param groupids [optional] Only include groups with IDs or names in
     * this list. By default, all modified groups will be included.
     * @param includeCancelled  [optional] Include cancelled groups. By
     * default, cancelled groups are excluded.
     * @param membershipChanges [optional] Include groups whose members have
     * changed. By default, changes to group memberships are not taken into
     * consideration.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The modified groups (in groupid order).
     */
    public java.util.List<IbisGroup> modifiedGroups(long    minTxId,
                                                    long    maxTxId,
                                                    String  groupids,
                                                    boolean includeCancelled,
                                                    boolean membershipChanges,
                                                    String  fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "minTxId", minTxId,
                                 "maxTxId", maxTxId,
                                 "groupids", groupids,
                                 "includeCancelled", includeCancelled,
                                 "membershipChanges", membershipChanges,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/modified-groups",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.groups;
    }

    /**
     * Search for groups using a free text query string. This is the same
     * search function that is used in the Lookup web application.
     * <p>
     * By default, only a few basic details about each group are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * NOTE: If the query string starts with the prefix {@code "group:"}, it
     * is treated as an <a href="/lql" target="_top">LQL query</a>, allowing
     * more advanced searches. An LQL query will ignore the
     * <code>approxMatches</code> parameter, but it will respect the value of
     * <code>includeCancelled</code>. In addition, an LQL query will ignore
     * the <code>orderBy</code> parameter, since LQL queries always return
     * results in ID order.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/search?query=... ]</code>
     *
     * @param query [required] The search string.
     * @param approxMatches [optional] Flag to enable more approximate
     * matching in the search, causing more results to be returned. Defaults
     * to {@code false}. This is ignored for LQL queries.
     * @param includeCancelled [optional] Flag to allow cancelled groups to
     * be included. Defaults to {@code false}.
     * @param offset [optional] The number of results to skip at the start
     * of the search. Defaults to 0.
     * @param limit [optional] The maximum number of results to return.
     * Defaults to 100.
     * @param orderBy [optional] The order in which to list the results.
     * This may be {@code "groupid"}, {@code "name"} (the default for non-LQL
     * queries) or {@code "title"}. This is ignored for LQL queries, which
     * always return results in groupid order.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The matching groups.
     */
    public java.util.List<IbisGroup> search(String  query,
                                            boolean approxMatches,
                                            boolean includeCancelled,
                                            int     offset,
                                            int     limit,
                                            String  orderBy,
                                            String  fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "query", query,
                                 "approxMatches", approxMatches,
                                 "includeCancelled", includeCancelled,
                                 "offset", offset,
                                 "limit", limit,
                                 "orderBy", orderBy,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/search",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.groups;
    }

    /**
     * Count the number of groups that would be returned by a search using
     * a free text query string.
     * <p>
     * NOTE: If the query string starts with the prefix {@code "group:"}, it
     * is treated as an <a href="/lql" target="_top">LQL query</a>, allowing
     * more advanced searches. An LQL query will ignore the
     * <code>approxMatches</code> parameter, but it will respect the value of
     * <code>includeCancelled</code>.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/search-count?query=... ]</code>
     *
     * @param query [required] The search string.
     * @param approxMatches [optional] Flag to enable more approximate
     * matching in the search, causing more results to be returned. Defaults
     * to {@code false}. This is ignored for LQL queries.
     * @param includeCancelled [optional] Flag to allow cancelled groups to
     * be included. Defaults to {@code false}.
     *
     * @return The number of matching groups.
     */
    public int searchCount(String   query,
                           boolean  approxMatches,
                           boolean  includeCancelled)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "query", query,
                                 "approxMatches", approxMatches,
                                 "includeCancelled", includeCancelled };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/search-count",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return Integer.parseInt(result.value);
    }

    /**
     * Get the group with the specified ID or name.
     * <p>
     * By default, only a few basic details about the group are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of the group.
     * <p>
     * NOTE: The group returned may be a cancelled group. It is the caller's
     * responsibility to check its cancelled flag.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/{groupid} ]</code>
     *
     * @param groupid [required] The ID or name of the group to fetch. This
     * may be either the numeric ID or the short hyphenated group name (for
     * example {@code "100656"} or {@code "cs-editors"}).
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The requested group or {@code null} if it was not found.
     */
    public IbisGroup getGroup(String    groupid,
                              String    fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { groupid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.group;
    }

    /**
     * Get all the cancelled members of the specified group, including
     * cancelled members of groups included by the group, and groups included
     * by those groups, and so on.
     * <p>
     * By default, only a few basic details about each member are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of each person.
     * <p>
     * NOTE: This method returns only cancelled people. It does not include
     * people who were removed from the group. Cancelled people are no longer
     * considered to be current staff, students or accredited visitors, and
     * are no longer regarded as belonging to any groups or institutions. The
     * list returned here reflects their group memberships just before they
     * were cancelled, and so is out-of-date data that should be used with
     * caution.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/{groupid}/cancelled-members ]</code>
     *
     * @param groupid [required] The ID or name of the group.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch for each person.
     *
     * @return The group's cancelled members (in identifier order).
     */
    public java.util.List<IbisPerson> getCancelledMembers(String    groupid,
                                                          String    fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { groupid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/%1$s/cancelled-members",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Get the direct members of the specified group, not including members
     * included via groups included by the group.
     * <p>
     * By default, only a few basic details about each member are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of each person.
     * <p>
     * NOTE: This method will not include cancelled people.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/{groupid}/direct-members ]</code>
     *
     * @param groupid [required] The ID or name of the group.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch for each person.
     *
     * @return The group's direct members (in identifier order).
     */
    public java.util.List<IbisPerson> getDirectMembers(String   groupid,
                                                       String   fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { groupid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/%1$s/direct-members",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Update the list of people who are direct members of the group. This
     * will not affect people who are included in the group due to the
     * inclusion of other groups.
     * <p>
     * Any non-cancelled people in the list of identifiers specified by
     * {@code addIds} will be added to the group. This list should be a
     * comma-separated list of identifiers, each of which may be either a
     * CRSid or an identifier from another identifier scheme, prefixed with
     * that scheme's name and a slash. For example {@code "mug99"} or
     * {@code "usn/123456789"}.
     * <p>
     * Any people in the list of identifiers specified by {@code removeIds}
     * will be removed from the group, except if they are also in the list
     * {@code addIds}. The special identifier {@code "all-members"} may be
     * used to remove all existing group members, replacing them with the
     * list specified by {@code newIds}.
     * <p>
     * <h5>Examples:</h5>
     * <pre>
     * updateDirectMembers("test-group",
     *                     "mug99,crsid/yyy99,usn/123456789",
     *                     "xxx99",
     *                     "Remove xxx99 and add mug99, yyy99 and usn/123456789 to test-group");
     * </pre>
     * <pre>
     * updateDirectMembers("test-group",
     *                     "xxx99,yyy99",
     *                     "all-members",
     *                     "Set the membership of test-group to include only xxx99 and yyy99");
     * </pre>
     * <p>
     * <code style="background-color: #eec;">[ HTTP: PUT /api/v1/group/{groupid}/direct-members ]</code>
     *
     * @param groupid [required] The ID or name of the group.
     * @param addIds [optional] The identifiers of people to add to the group.
     * @param removeIds [optional] The identifiers of people to remove from
     * the group.
     * @param commitComment [recommended] A short textual description of
     * the change made (will be visible on the history tab of the group and
     * all the affected people in the web application).
     *
     * @return The updated list of direct members of the group (in identifier
     * order).
     */
    public java.util.List<IbisPerson> updateDirectMembers(String    groupid,
                                                          String    addIds,
                                                          String    removeIds,
                                                          String    commitComment)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { groupid };
        Object[] queryParams = {  };
        Object[] formParams = { "addIds", addIds,
                                "removeIds", removeIds,
                                "commitComment", commitComment };
        IbisResult result = conn.invokeMethod(Method.PUT,
                                              "api/v1/group/%1$s/direct-members",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Get all the members of the specified group, including members of
     * groups included by the group, and groups included by those groups,
     * and so on.
     * <p>
     * By default, only a few basic details about each member are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of each person.
     * <p>
     * NOTE: This method will not include cancelled people.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/{groupid}/members ]</code>
     *
     * @param groupid [required] The ID or name of the group.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch for each person.
     *
     * @return The group's members (in identifier order).
     */
    public java.util.List<IbisPerson> getMembers(String groupid,
                                                 String fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { groupid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/%1$s/members",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Get a signed JSON Web Token (JWT) with the group as subject and
     * specified audience, only if authorized user/group has permission to edit
     * the group.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/group/{groupid}/token?aud=... ]</code>
     *
     * @param groupid [required] The ID of the group.
     * @param aud [required] Audience for the signed JWT.
     *
     * @return The serialized JWT
     */
    public String getToken(String   groupid,
                           String   aud)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { groupid };
        Object[] queryParams = { "aud", aud };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/group/%1$s/token",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.value;
    }
}
