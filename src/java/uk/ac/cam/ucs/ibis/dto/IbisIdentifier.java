/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Class representing a person's identifier, for use by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class IbisIdentifier
{
    /** The identifier's scheme (e.g., "crsid"). */
    @XmlAttribute public String scheme;

    /** The identifier's value in that scheme (e.g., a specific CRSid value). */
    @XmlValue public String value;

    @Override
    public String toString() {
        // Canonical string representation of identifiers (used in the
        // "id" attribute for people in XML/JSON)
        return "crsid".equals(scheme) ? value : scheme+"/"+value;
    }
}
