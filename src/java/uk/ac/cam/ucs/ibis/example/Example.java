/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.example;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.ibis.client.*;
import uk.ac.cam.ucs.ibis.dto.*;
import uk.ac.cam.ucs.ibis.methods.*;

/**
 * Example class to demonstrate how to use the Lookup/Ibis web service
 * client API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class Example
{
    /**
     * Run the example.
     */
    public static void main(String[]    args)
        throws GeneralSecurityException, IbisException, IOException, JAXBException
    {
        // The following setup need only be run once. First create a
        // ClientConnection object that can connect to the Lookup/Ibis
        // server. This is not a persistent connection, but rather it
        // automatically connects to the server as needed.
        //
        // To connect to the live server on www.lookup.cam.ac.uk use:
        // ClientConnection conn = IbisClientConnection.createConnection();
        // or to connect to the test server on lookup-test.srv.uis.cam.ac.uk:
        ClientConnection conn = IbisClientConnection.createTestConnection();

        // Create instances of the method classes, depending on which
        // API methods you require. Again this need only be done once.
        //
        // PersonMethods pm = new PersonMethods(conn);
        InstitutionMethods im = new InstitutionMethods(conn);
        // GroupMethods gm = new GroupMethods(conn);

        // Invoke a web service method (this will fetch all the members of
        // the Computing Service)
        List<IbisPerson> people = im.getMembers("UIS", null);

        // Print the results
        System.out.println("Members of University Information Services:");
        for (IbisPerson person : people)
            System.out.println(" - "+person.identifier.value+": "+person.visibleName);
    }
}
