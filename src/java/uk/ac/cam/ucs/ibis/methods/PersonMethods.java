/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.ibis.client.ClientConnection;
import uk.ac.cam.ucs.ibis.client.ClientConnection.Method;
import uk.ac.cam.ucs.ibis.client.IbisException;
import uk.ac.cam.ucs.ibis.dto.*;

/**
 * Methods for querying and manipulating people.
 *
 * <h4>Notes on the fetch parameter</h4>
 * <p>
 * All methods that return people, institutions or groups also accept an
 * optional <code>fetch</code> parameter that may be used to request
 * additional information about the entities returned. Without this
 * parameter, only a few basic details about each person, institution or
 * group are returned. The <code>fetch</code> parameter is quite flexible,
 * and may be used in a number of different ways:
 *
 * <ul>
 * <li>
 * <b>Attribute fetching</b>. Attributes may be fetched by specifying the
 * <code>schemeid</code> of an attribute scheme. For example to fetch a
 * person's email addresses, use the value {@code "email"}. For people common
 * attribute schemes include {@code "jpegPhoto"}, {@code "misAffiliation"},
 * {@code "title"}, {@code "universityPhone"}, {@code "mobexPhone"},
 * {@code "landlinePhone"}, {@code "mobilePhone"}, {@code "pager"},
 * {@code "labeledURI"} and {@code "address"}. The full list of person
 * attribute schemes may be obtained using {@link #allAttributeSchemes}.
 * <br/><br/>
 * </li>
 *
 * <li>
 * <b>Pseudo-attributes</b>. Certain special pseudo-attributes are defined
 * for convenience. For people, the following pseudo-attributes are supported:
 * <ul>
 * <li>{@code "phone_numbers"} - fetches all phone numbers. This is
 * equivalent to
 * {@code "universityPhone,instPhone,mobexPhone,landlinePhone,mobilePhone,pager"}.</li>
 * <li>{@code "all_identifiers"} - fetches all identifiers. Currently people
 * only have CRSid identifiers, but in the future additional identifiers such
 * as USN or staffNumber may be added.</li>
 * <li>{@code "all_attrs"} - fetches all attributes from all person attribute
 * schemes. This does not include identifiers or references.</li>
 * </ul>
 * <br/><br/>
 * </li>
 *
 * <li>
 * <b>Reference fetching</b>. For people, the following references are
 * supported (and will fetch only non-cancelled institutions and groups):
 * <ul>
 * <li>{@code "all_insts"} - fetches all the institutions to which the person
 * belongs (sorted in name order).</li>
 * <li>{@code "all_groups"} - fetches all the groups that the person is a
 * member of, including indirect group memberships, via groups that include
 * other groups.</li>
 * <li>{@code "direct_groups"} - fetches all the groups that the person is
 * directly a member of. This does not include indirect group memberships -
 * i.e., groups that include these groups.</li>
 * </ul>
 * <br/><br/>
 * </li>
 *
 * <li>
 * <b>Chained reference fetching</b>. To fetch properties of referenced
 * objects, the "dot" notation may be used. For example, to fetch the email
 * addresses of all the institutions to which a person belongs, use
 * {@code "all_insts.email"}. Chains may include a number of reference
 * following steps, for example
 * {@code "all_insts.managed_by_groups.all_members.email"} will fetch all the
 * institutions to which the person belongs, all the groups that manage those
 * institutions, all the visible members of those groups and all the email
 * addresses of those managing group members. For more information about what
 * can be fetched from referenced institutions and groups, refer to the
 * documentation for {@link InstitutionMethods} and {@link GroupMethods}.
 * </li>
 * </ul>
 *
 * <p>
 * Multiple values of the <code>fetch</code> parameter should be separated
 * by commas.
 *
 * <h4>Fetch parameter examples</h4>
 * <p>
 * <code>fetch = "email"</code><br/>
 * This fetches all the person's email addresses.
 * <p>
 * <code>fetch = "title,address"</code><br/>
 * This fetches all the person's titles (roles) and addresses.
 * <p>
 * <code>fetch = "all_attrs"</code><br/>
 * This fetches all the person's attributes.
 * <p>
 * <code>fetch = "all_groups,all_insts"</code><br/>
 * This fetches all the groups and institutions to which the person belongs.
 * <p>
 * <code>fetch = "all_insts.parent_insts"</code><br/>
 * This fetches all the person's institutions, and their parent institutions.
 * <p>
 * <code>fetch = "all_insts.email,all_insts.all_members.email"</code><br/>
 * This fetches all the person's institutions and their email addresses, and
 * all the members of those institutions, and the email addresses of all
 * those members.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class PersonMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new PersonMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public PersonMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Return a list of all the person attribute schemes available. The
     * {@code schemeid} values of these schemes may be used in the
     * <code>fetch</code> parameter of other methods that return people.
     * <p>
     * NOTE: Some of these attribute schemes are not currently used (no
     * people have attribute values in the scheme). These schemes are
     * reserved for possible future use.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/all-attr-schemes ]</code>
     *
     * @return All the available person attribute schemes (in precedence
     * order).
     */
    public java.util.List<IbisAttributeScheme> allAttributeSchemes()
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/all-attr-schemes",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attributeSchemes;
    }

    /**
     * Return a list of all people (in batches).
     * <p>
     * The results are sorted by identifier, starting with the first person
     * after the person with the specified identifier. Thus, to iterate over
     * all people, pass a {@code null} identifier to get the first batch of
     * people, then pass the last identifier from the previous batch to get
     * the next batch, and repeat until no more people are returned.
     * <p>
     * By default, only a few basic details about each person are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/all-people ]</code>
     *
     * @param includeCancelled [optional] Flag to allow cancelled people to
     * be included (people who are no longer members of the University).
     * Defaults to {@code false}.
     * @param identifier [optional] The identifier (CRSid) of the person to
     * start after, or {@code null} to start from the first person.
     * @param limit [optional] The maximum number of people to return.
     * Defaults to 100.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The requested people (in identifier order).
     */
    public java.util.List<IbisPerson> allPeople(boolean includeCancelled,
                                                String  identifier,
                                                int     limit,
                                                String  fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "includeCancelled", includeCancelled,
                                 "identifier", identifier,
                                 "limit", limit,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/all-people",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Get the people with the specified identifiers (typically CRSids).
     * <p>
     * Each identifier may be either a CRSid, or an identifier from another
     * identifier scheme, prefixed with that scheme's name and a slash. For
     * example {@code "mug99"} or {@code "usn/123456789"}.
     * <p>
     * By default, only a few basic details about each person are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * The results are sorted by identifier scheme and value.
     * <p>
     * NOTE: The number of people that may be fetched in a single call is
     * limited by the URL path length limit (around 8000 characters). A
     * CRSid is up to 7 characters long, and other identifiers are typically
     * longer, since they must also include the identifier scheme. Thus the
     * number of people that this method may fetch is typically limited to a
     * few hundred.
     * <p>
     * NOTE: The people returned may include cancelled people. It is the
     * caller's repsonsibility to check their cancelled flags.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/list?crsids=... ]</code>
     *
     * @param crsids [required] A comma-separated list of identifiers. The name
     * of the query parameter reflects a time when only crsids were used with
     * lookup. Alternate schemes can be specified as noted above.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The requested people (in identifier order).
     */
    public java.util.List<IbisPerson> listPeople(String crsids,
                                                 String fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "crsids", crsids,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/list",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Find all people modified between the specified pair of transactions.
     * <p>
     * The transaction IDs specified should be the IDs from two different
     * requests for the last (most recent) transaction ID, made at different
     * times, that returned different values, indicating that some Lookup
     * data was modified in the period between the two requests. This method
     * then determines which (if any) people were affected.
     * <p>
     * By default, only a few basic details about each person are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * NOTE: All data returned reflects the latest available data about each
     * person. It is not possible to query for old data, or more detailed
     * information about the specific changes made.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/modified-people?minTxId=...&maxTxId=... ]</code>
     *
     * @param minTxId [required] Include modifications made in transactions
     * after (but not including) this one.
     * @param maxTxId [required] Include modifications made in transactions
     * up to and including this one.
     * @param crsids [optional] Only include people with identifiers in this
     * list. By default, all modified people will be included.
     * @param includeCancelled  [optional] Include cancelled people (people
     * who are no longer members of the University). By default, cancelled
     * people are excluded.
     * @param membershipChanges [optional] Include people whose group or
     * institutional memberships have changed. By default, only people whose
     * attributes have been directly modified are included.
     * @param instNameChanges [optional] Include people who are members of
     * instituions whose names have changed. This will also cause people
     * whose group or institutional memberships have changed to be included.
     * By default, changes to institution names do not propagate to people.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The modified people (in identifier order).
     */
    public java.util.List<IbisPerson> modifiedPeople(long       minTxId,
                                                     long       maxTxId,
                                                     String     crsids,
                                                     boolean    includeCancelled,
                                                     boolean    membershipChanges,
                                                     boolean    instNameChanges,
                                                     String     fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "minTxId", minTxId,
                                 "maxTxId", maxTxId,
                                 "crsids", crsids,
                                 "includeCancelled", includeCancelled,
                                 "membershipChanges", membershipChanges,
                                 "instNameChanges", instNameChanges,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/modified-people",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Search for people using a free text query string. This is the same
     * search function that is used in the Lookup web application.
     * <p>
     * By default, only a few basic details about each person are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * NOTE: If the query string starts with the prefix {@code "person:"}, it
     * is treated as an <a href="/lql" target="_top">LQL query</a>, allowing
     * more advanced searches. An LQL query will ignore the
     * <code>approxMatches</code> and <code>attributes</code> parameters, but
     * it will respect the values of <code>includeCancelled</code> and
     * <code>misStatus</code>. In addition, an LQL query will ignore the
     * <code>orderBy</code> parameter, since LQL queries always return
     * results in ID order.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/search?query=... ]</code>
     *
     * @param query [required] The search string.
     * @param approxMatches [optional] Flag to enable more approximate
     * matching in the search, causing more results to be returned. Defaults
     * to {@code false}. This is ignored for LQL queries.
     * @param includeCancelled [optional] Flag to allow cancelled people to
     * be included (people who are no longer members of the University).
     * Defaults to {@code false}.
     * @param misStatus [optional] The type of people to search for. This may
     * be
     * <ul>
     * <li>{@code "staff"} - only include people whose MIS status is
     * {@code ""} (empty string), {@code "staff"}, or
     * {@code "staff,student"}.</li>
     * <li>{@code "student"} - only include people whose MIS status is set to
     * {@code "student"} or {@code "staff,student"}.</li>
     * </ul>
     * Otherwise all matching people will be included (the default). Note
     * that the {@code "staff"} and {@code "student"} options are not
     * mutually exclusive.
     * @param attributes [optional] A comma-separated list of attributes to
     * consider when searching. If this is {@code null} (the default) then
     * all attribute schemes marked as searchable will be included. This is
     * ignored for LQL queries.
     * @param offset [optional] The number of results to skip at the start
     * of the search. Defaults to 0.
     * @param limit [optional] The maximum number of results to return.
     * Defaults to 100.
     * @param orderBy [optional] The order in which to list the results.
     * This may be either {@code "identifier"} or {@code "surname"} (the
     * default for non-LQL queries). This is ignored for LQL queries, which
     * always return results in identifier order.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The matching people.
     */
    public java.util.List<IbisPerson> search(String     query,
                                             boolean    approxMatches,
                                             boolean    includeCancelled,
                                             String     misStatus,
                                             String     attributes,
                                             int        offset,
                                             int        limit,
                                             String     orderBy,
                                             String     fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "query", query,
                                 "approxMatches", approxMatches,
                                 "includeCancelled", includeCancelled,
                                 "misStatus", misStatus,
                                 "attributes", attributes,
                                 "offset", offset,
                                 "limit", limit,
                                 "orderBy", orderBy,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/search",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Count the number of people that would be returned by a search using
     * a free text query string.
     * <p>
     * NOTE: If the query string starts with the prefix {@code "person:"}, it
     * is treated as an <a href="/lql" target="_top">LQL query</a>, allowing
     * more advanced searches. An LQL query will ignore the
     * <code>approxMatches</code> and <code>attributes</code> parameters, but
     * it will respect the values of <code>includeCancelled</code> and
     * <code>misStatus</code>.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/search-count?query=... ]</code>
     *
     * @param query [required] The search string.
     * @param approxMatches [optional] Flag to enable more approximate
     * matching in the search, causing more results to be returned. Defaults
     * to {@code false}. This is ignored for LQL queries.
     * @param includeCancelled [optional] Flag to allow cancelled people to
     * be included (people who are no longer members of the University).
     * Defaults to {@code false}.
     * @param misStatus [optional] The type of people to search for. This may
     * be
     * <ul>
     * <li>{@code "staff"} - only include people whose MIS status is
     * {@code ""} (empty string), {@code "staff"}, or
     * {@code "staff,student"}.</li>
     * <li>{@code "student"} - only include people whose MIS status is set to
     * {@code "student"} or {@code "staff,student"}.</li>
     * </ul>
     * Otherwise all matching people will be included (the default). Note
     * that the {@code "staff"} and {@code "student"} options are not
     * mutually exclusive.
     * @param attributes [optional] A comma-separated list of attributes to
     * consider when searching. If this is {@code null} (the default) then
     * all attribute schemes marked as searchable will be included. This is
     * ignored for LQL queries.
     *
     * @return The number of matching people.
     */
    public int searchCount(String   query,
                           boolean  approxMatches,
                           boolean  includeCancelled,
                           String   misStatus,
                           String   attributes)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "query", query,
                                 "approxMatches", approxMatches,
                                 "includeCancelled", includeCancelled,
                                 "misStatus", misStatus,
                                 "attributes", attributes };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/search-count",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return Integer.parseInt(result.value);
    }

    /**
     * Get the person with the specified identifier.
     * <p>
     * By default, only a few basic details about the person are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of the person.
     * <p>
     * NOTE: The person returned may be a cancelled person. It is the
     * caller's repsonsibility to check its cancelled flag.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{scheme}/{identifier} ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes
     * such as {@code "usn"} or {@code "staffNumber"} may be available.
     * @param identifier [required] The identifier of the person to fetch
     * (typically their CRSid).
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The requested person or {@code null} if they were not found.
     */
    public IbisPerson getPerson(String  scheme,
                                String  identifier,
                                String  fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/%2$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.person;
    }

    /**
     * Add an attribute to a person. By default, this will not add the
     * attribute again if it already exists.
     * <p>
     * When adding an attribute, the new attribute's scheme must be set.
     * In addition, either its value or its binaryData field should be set.
     * All the remaining fields of the attribute are optional.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: POST /api/v1/person/{scheme}/{identifier}/add-attribute ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person to udpate
     * (typically their CRSid).
     * @param attr [required] The new attribute to add.
     * @param position [optional] The position of the new attribute in the
     * list of attributes of the same attribute scheme (1, 2, 3,...). A value
     * of 0 (the default) will cause the new attribute to be added to the end
     * of the list of existing attributes for the scheme.
     * @param allowDuplicates [optional] If {@code true}, the new attribute
     * will always be added, even if another identical attribute already
     * exists. If {@code false} (the default), the new attribute will only be
     * added if it doesn't already exist.
     * @param commitComment [recommended] A short textual description of
     * the change made (will be visible on the history tab in the web
     * application).
     *
     * @return The newly created or existing attribute.
     */
    public IbisAttribute addAttribute(String        scheme,
                                      String        identifier,
                                      IbisAttribute attr,
                                      int           position,
                                      boolean       allowDuplicates,
                                      String        commitComment)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier };
        Object[] queryParams = {  };
        Object[] formParams = { "attr", attr,
                                "position", position,
                                "allowDuplicates", allowDuplicates,
                                "commitComment", commitComment };
        IbisResult result = conn.invokeMethod(Method.POST,
                                              "api/v1/person/%1$s/%2$s/add-attribute",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attribute;
    }

    /**
     * Get one or more (possibly multi-valued) attributes of a person. The
     * returned attributes are sorted by attribute scheme precedence and
     * then attribute precedence.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{scheme}/{identifier}/get-attributes?attrs=... ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person (typically
     * their CRSid).
     * @param attrs [required] The attribute scheme(s) to fetch. This may
     * include any number of the attributes or pseudo-attributes, but it
     * may not include references or attribute chains (see the documentation
     * for the {@code fetch} parameter in this class).
     *
     * @return The requested attributes.
     */
    public java.util.List<IbisAttribute> getAttributes(String   scheme,
                                                       String   identifier,
                                                       String   attrs)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier };
        Object[] queryParams = { "attrs", attrs };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/%2$s/get-attributes",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attributes;
    }

    /**
     * Get all the groups to which the specified person belongs, including
     * indirect group memberships, via groups that include other groups.
     * The returned list of groups is sorted by groupid.
     * <p>
     * Note that some group memberships may not be visible to you. This
     * method will only return those group memberships that you have
     * permission to see.
     * <p>
     * By default, only a few basic details about each group are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of each group.
     * <p>
     * NOTE: This method will not include cancelled groups.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{scheme}/{identifier}/groups ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person (typically
     * their CRSid).
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The person's groups (in groupid order).
     */
    public java.util.List<IbisGroup> getGroups(String   scheme,
                                               String   identifier,
                                               String   fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/%2$s/groups",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.groups;
    }

    /**
     * Get all the institutions to which the specified person belongs. The
     * returned list of institutions is sorted by name.
     * <p>
     * By default, only a few basic details about each institution are
     * returned, but the optional <code>fetch</code> parameter may be used
     * to fetch additional attributes or references of each institution.
     * <p>
     * NOTE: This method will not include cancelled institutions.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{scheme}/{identifier}/insts ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person (typically
     * their CRSid).
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The person's institutions (in name order).
     */
    public java.util.List<IbisInstitution> getInsts(String  scheme,
                                                    String  identifier,
                                                    String  fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/%2$s/insts",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.institutions;
    }

    /**
     * Test if the specified person is a member of the specified group.
     * <p>
     * NOTE: This may be used with cancelled people and groups.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{scheme}/{identifier}/is-member-of-group/{groupid} ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person (typically
     * their CRSid).
     * @param groupid [required] The ID or name of the group.
     *
     * @return {@code true} if the specified person is in the specified
     * group, {@code false} otherwise (or if the person or group does not
     * exist).
     */
    public boolean isMemberOfGroup(String   scheme,
                                   String   identifier,
                                   String   groupid)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier, groupid };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/%2$s/is-member-of-group/%3$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return Boolean.parseBoolean(result.value);
    }

    /**
     * Test if the specified person is a member of the specified institution.
     * <p>
     * NOTE: This may be used with cancelled people and institutions, but
     * it will not include cancelled membership groups.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{scheme}/{identifier}/is-member-of-inst/{instid} ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person (typically
     * their CRSid).
     * @param instid [required] The ID of the institution.
     *
     * @return {@code true} if the specified person is in the specified
     * institution, {@code false} otherwise (or if the person or institution
     * does not exist).
     */
    public boolean isMemberOfInst(String    scheme,
                                  String    identifier,
                                  String    instid)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier, instid };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/%2$s/is-member-of-inst/%3$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return Boolean.parseBoolean(result.value);
    }

    /**
     * Get all the groups that the specified person has persmission to edit.
     * The returned list of groups is sorted by groupid.
     * <p>
     * Note that some group memberships may not be visible to you. This
     * method will only include groups for which you have persmission to
     * see the applicable manager group memberships.
     * <p>
     * By default, only a few basic details about each group are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of each group.
     * <p>
     * NOTE: This method will not include cancelled groups.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{scheme}/{identifier}/manages-groups ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person (typically
     * their CRSid).
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The groups that the person manages (in groupid order).
     */
    public java.util.List<IbisGroup> getManagedGroups(String    scheme,
                                                      String    identifier,
                                                      String    fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/%2$s/manages-groups",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.groups;
    }

    /**
     * Get all the institutions that the specified person has permission to
     * edit. The returned list of institutions is sorted by name.
     * <p>
     * Note that some group memberships may not be visible to you. This
     * method will only include institutions for which you have permission
     * to see the applicable editor group memberships.
     * <p>
     * By default, only a few basic details about each institution are
     * returned, but the optional <code>fetch</code> parameter may be used
     * to fetch additional attributes or references of each institution.
     * <p>
     * NOTE: This method will not include cancelled institutions.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{scheme}/{identifier}/manages-insts ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person (typically
     * their CRSid).
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The institutions that the person manages (in name order).
     */
    public java.util.List<IbisInstitution> getManagedInsts(String   scheme,
                                                           String   identifier,
                                                           String   fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/%2$s/manages-insts",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.institutions;
    }

    /**
     * Delete an attribute of a person. It is not an error if the attribute
     * does not exist.
     * <p>
     * Note that in this method, the <code>commitComment</code> is passed
     * as a query parameter, rather than as a form parameter, for greater
     * client compatibility.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: DELETE /api/v1/person/{scheme}/{identifier}/{attrid} ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person to udpate
     * (typically their CRSid).
     * @param attrid [required] The ID of the attribute to delete.
     * @param commitComment [recommended] A short textual description of
     * the change made (will be visible on the history tab in the web
     * application).
     *
     * @return {@code true} if the attribute was deleted by this method, or
     * {@code false} if it did not exist.
     */
    public boolean deleteAttribute(String   scheme,
                                   String   identifier,
                                   Long     attrid,
                                   String   commitComment)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier, ""+attrid };
        Object[] queryParams = { "commitComment", commitComment };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.DELETE,
                                              "api/v1/person/%1$s/%2$s/%3$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return Boolean.parseBoolean(result.value);
    }

    /**
     * Get a specific attribute of a person.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{scheme}/{identifier}/{attrid} ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person (typically
     * their CRSid).
     * @param attrid [required] The ID of the attribute to fetch.
     *
     * @return The requested attribute.
     */
    public IbisAttribute getAttribute(String    scheme,
                                      String    identifier,
                                      Long      attrid)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier, ""+attrid };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/%2$s/%3$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attribute;
    }

    /**
     * Update an attribute of a person.
     * <p>
     * The attribute's value, binaryData, comment, instid and effective date
     * fields will all be updated using the data supplied. All other fields
     * will be left unchanged.
     * <p>
     * To avoid inadvertently changing fields of the attribute, it is
     * recommended that {@link #getAttribute getAttribute()} be used to
     * retrieve the current value of the attribute, before calling this
     * method with the required changes.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: PUT /api/v1/person/{scheme}/{identifier}/{attrid} ]</code>
     *
     * @param scheme [required] The person identifier scheme. Typically this
     * should be {@code "crsid"}, but other identifier schemes may be
     * available in the future, such as {@code "usn"} or
     * {@code "staffNumber"}.
     * @param identifier [required] The identifier of the person to udpate
     * (typically their CRSid).
     * @param attrid [required] The ID of the attribute to update.
     * @param attr [required] The new attribute values to apply.
     * @param commitComment [recommended] A short textual description of
     * the change made (will be visible on the history tab in the web
     * application).
     *
     * @return The updated attribute.
     */
    public IbisAttribute updateAttribute(String         scheme,
                                         String         identifier,
                                         Long           attrid,
                                         IbisAttribute  attr,
                                         String         commitComment)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { scheme, identifier, ""+attrid };
        Object[] queryParams = {  };
        Object[] formParams = { "attr", attr,
                                "commitComment", commitComment };
        IbisResult result = conn.invokeMethod(Method.PUT,
                                              "api/v1/person/%1$s/%2$s/%3$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attribute;
    }
}
