/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.ibis.dto.IbisResult.EntityMap;

/**
 * Class representing an institution returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class IbisInstitution
{
    /** Flag indicating if the institution is cancelled. */
    @XmlAttribute public Boolean cancelled;

    /** The institution's unique ID (e.g., "CS"). */
    @XmlAttribute public String instid;

    /** The institution's name. */
    @XmlElement public String name;

    /** The institution's acronym, if set (e.g., "UCS"). */
    @XmlElement public String acronym;

    /**
     * A list of the institution's attributes. This will only be populated
     * if the <code>fetch</code> parameter includes the {@code "all_attrs"}
     * option, or any specific attribute schemes such as {@code "email"} or
     * {@code "address"}, or the special pseudo-attribute scheme
     * {@code "phone_numbers"}.
     */
    @XmlElementWrapper
    @XmlElement(name = "attribute")
    public List<IbisAttribute> attributes;

    /**
     * A list of the institution's contact rows. This will only be populated
     * if the <code>fetch</code> parameter includes the
     * {@code "contact_rows"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "contactRow")
    public List<IbisContactRow> contactRows;

    /**
     * A list of the institution's members. This will only be populated if
     * the <code>fetch</code> parameter includes the {@code "all_members"}
     * option.
     */
    @XmlElementWrapper
    @XmlElement(name = "person")
    public List<IbisPerson> members;

    /**
     * A list of the institution's parent institutions. This will only be
     * populated if the <code>fetch</code> parameter includes the
     * {@code "parent_insts"} option.
     * <p>
     * NOTE: Currently all institutions have one parent, but in the future
     * institutions may have multiple parents.
     */
    @XmlElementWrapper
    @XmlElement(name = "institution")
    public List<IbisInstitution> parentInsts;

    /**
     * A list of the institution's child institutions. This will only be
     * populated if the <code>fetch</code> parameter includes the
     * {@code "child_insts"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "institution")
    public List<IbisInstitution> childInsts;

    /**
     * A list of all the groups that belong to the institution. This will
     * only be populated if the <code>fetch</code> parameter includes the
     * {@code "inst_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> groups;

    /**
     * A list of the groups that form the institution's membership. This
     * will only be populated if the <code>fetch</code> parameter includes
     * the {@code "members_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> membersGroups;

    /**
     * A list of the groups that manage this institution. This will only
     * be populated if the <code>fetch</code> parameter includes the
     * {@code "managed_by_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> managedByGroups;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this institution within the
     * returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to an institution element in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "inst/"+instid; }

    /** Return a new IbisInstitution that refers to this institution. */
    public IbisInstitution ref() { IbisInstitution i = new IbisInstitution(); i.ref = id; return i; }

    /* == Code to help clients unflatten an IbisResult object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single IbisInstitution. */
    protected IbisInstitution unflatten(EntityMap   em)
    {
        if (ref != null)
        {
            IbisInstitution inst = em.getInstitution(ref);
            if (!inst.unflattened)
            {
                inst.unflattened = true;
                IbisContactRow.unflatten(em, inst.contactRows);
                IbisPerson.unflatten(em, inst.members);
                IbisInstitution.unflatten(em, inst.parentInsts);
                IbisInstitution.unflatten(em, inst.childInsts);
                IbisGroup.unflatten(em, inst.groups);
                IbisGroup.unflatten(em, inst.membersGroups);
                IbisGroup.unflatten(em, inst.managedByGroups);
            }
            return inst;
        }
        return this;
    }

    /** Unflatten a list of IbisInstitution objects (done in place). */
    protected static void unflatten(EntityMap               em,
                                    List<IbisInstitution>   insts)
    {
        if (insts != null)
            for (int i=0; i<insts.size(); i++)
                insts.set(i, insts.get(i).unflatten(em));

    }
}
