/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.ibis.dto.IbisResult.EntityMap;

/**
 * Class representing a group returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class IbisGroup
{
    /** Flag indicating if the group is cancelled. */
    @XmlAttribute public Boolean cancelled;

    /** The group's numeric ID (actually a string e.g., "100656"). */
    @XmlAttribute public String groupid;

    /** The group's unique name (e.g., "cs-editors"). */
    @XmlElement public String name;

    /** The group's title. */
    @XmlElement public String title;

    /** The more detailed description of the group. */
    @XmlElement public String description;

    /** The group's email address. */
    @XmlElement public String email;

    /**
     * The details of the institution for which this group forms all or
     * part of the membership. This will only be set for groups that are
     * membership groups of institutions if the <code>fetch</code> parameter
     * includes the {@code "members_of_inst"} option.
     */
    @XmlElement public IbisInstitution membersOfInst;

    /**
     * A list of the group's members, including (recursively) any members of
     * any included groups. This will only be populated if the
     * <code>fetch</code> parameter includes the {@code "all_members"}
     * option.
     */
    @XmlElementWrapper
    @XmlElement(name = "person")
    public List<IbisPerson> members;

    /**
     * A list of the group's direct members, not including any members
     * included via groups included by this group. This will only be
     * populated if the <code>fetch</code> parameter includes the
     * {@code "direct_members"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "person")
    public List<IbisPerson> directMembers;

    /**
     * A list of the institutions to which this group belongs. This will only
     * be populated if the <code>fetch</code> parameter includes the
     * {@code "owning_insts"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "institution")
    public List<IbisInstitution> owningInsts;

    /**
     * A list of the institutions managed by this group. This will only be
     * populated if the <code>fetch</code> parameter includes the
     * {@code "manages_insts"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "institution")
    public List<IbisInstitution> managesInsts;

    /**
     * A list of the groups managed by this group. This will only be
     * populated if the <code>fetch</code> parameter includes the
     * {@code "manages_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> managesGroups;

    /**
     * A list of the groups that manage this group. This will only be
     * populated if the <code>fetch</code> parameter includes the
     * {@code "managed_by_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> managedByGroups;

    /**
     * A list of the groups that this group has privileged access to. Members
     * of this group will be able to read the members of any of those groups,
     * regardless of the membership visibilities. This will only be populated
     * if the <code>fetch</code> parameter includes the
     * {@code "reads_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> readsGroups;

    /**
     * A list of the groups that have privileged access to this group.
     * Members of those groups will be able to read the members of this
     * group, regardless of the membership visibilities. This will only be
     * populated if the <code>fetch</code> parameter includes the
     * {@code "read_by_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> readByGroups;

    /**
     * A list of the groups directly included in this group. Any members of
     * the included groups (and recursively any groups that they include)
     * will automatically be included in this group. This will only be
     * populated if the <code>fetch</code> parameter includes the
     * {@code "includes_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> includesGroups;

    /**
     * A list of the groups that directly include this group. Any members of
     * this group will automatically be included in those groups (and
     * recursively in any groups that include those groups). This will only
     * be populated if the <code>fetch</code> parameter includes the
     * {@code "included_by_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> includedByGroups;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this group within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to a group element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "group/"+groupid; }

    /** Return a new IbisGroup that refers to this group. */
    public IbisGroup ref() { IbisGroup g = new IbisGroup(); g.ref = id; return g; }

    /* == Code to help clients unflatten an IbisResult object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single IbisGroup. */
    protected IbisGroup unflatten(EntityMap em)
    {
        if (ref != null)
        {
            IbisGroup group = em.getGroup(ref);
            if (!group.unflattened)
            {
                group.unflattened = true;
                if (group.membersOfInst != null)
                    group.membersOfInst = group.membersOfInst.unflatten(em);
                IbisPerson.unflatten(em, group.members);
                IbisPerson.unflatten(em, group.directMembers);
                IbisInstitution.unflatten(em, group.owningInsts);
                IbisInstitution.unflatten(em, group.managesInsts);
                IbisGroup.unflatten(em, group.managesGroups);
                IbisGroup.unflatten(em, group.managedByGroups);
                IbisGroup.unflatten(em, group.readsGroups);
                IbisGroup.unflatten(em, group.readByGroups);
                IbisGroup.unflatten(em, group.includesGroups);
                IbisGroup.unflatten(em, group.includedByGroups);
            }
            return group;
        }
        return this;
    }

    /** Unflatten a list of IbisGroup objects (done in place). */
    protected static void unflatten(EntityMap       em,
                                    List<IbisGroup> groups)
    {
        if (groups != null)
            for (int i=0; i<groups.size(); i++)
                groups.set(i, groups.get(i).unflatten(em));

    }
}
