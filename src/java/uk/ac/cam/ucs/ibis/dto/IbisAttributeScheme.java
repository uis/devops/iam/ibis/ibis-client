/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Class representing an attribute scheme. This may apply to attributes of
 * people or institutions.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class IbisAttributeScheme
{
    /** The unique identifier of the attribute scheme. */
    @XmlAttribute public String schemeid;

    /**
     * The attribute scheme's precedence. Methods that return or display
     * attributes sort the results primarily in order of increasing values
     * of attribute scheme precedence.
     */
    @XmlAttribute public int precedence;

    /**
     * The name of the attribute scheme in LDAP, if it is exported to LDAP.
     * Note that many attributes are not exported to LDAP, in which case
     * this name is typically just equal to the scheme's ID.
     */
    @XmlElement public String ldapName;

    /** The display name for labelling attributes in this scheme. */
    @XmlElement public String displayName;

    /** The attribute scheme's datatype. */
    @XmlElement public String dataType;

    /** Flag indicating whether attributes in this scheme can be multi-valued. */
    @XmlAttribute public boolean multiValued;

    /**
     * Flag for textual attributes schemes indicating whether they are
     * multi-lined.
     */
    @XmlAttribute public boolean multiLined;

    /**
     * Flag indicating whether attributes of this scheme are searched by
     * the default search functionality.
     */
    @XmlAttribute public boolean searchable;

    /**
     * For textual attributes, an optional regular expression that all
     * attributes in this scheme match.
     */
    @XmlElement public String regexp;
}
