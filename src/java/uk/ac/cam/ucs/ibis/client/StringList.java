/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.client;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Utility class to hold a list of strings and convert them to a
 * comma-separated string, as required by various web service API methods.
 * <p>
 * Note that the {@link #toString()} method just joins the strings together
 * using commas with no escaping. This will give unexpected results if any
 * of the strings in the list contains commas, but that should not be the
 * case for any of the strings used by the web service API methods.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class StringList extends ArrayList<String>
{
    private static final long serialVersionUID = 1862415137171608905L;

    /**
     * Constructs an empty string list with the specified initial capacity.
     *
     * @param initialCapacity the initial capacity of the list.
     */
    public StringList(int initialCapacity)
    {
        super(initialCapacity);
    }

    /**
     * Constructs an empty string list with an initial capacity of ten.
     */
    public StringList()
    {
        super();
    }

    /**
     * Constructs a string list containing the elements of the specified
     * collection, in the order they are returned by the collection's
     * iterator.
     *
     * @param c the collection whose elements are to be placed into this
     * list.
     */
    public StringList(Collection<String> c)
    {
        super(c);
    }

    /**
     * Constructs a string list from the specified strings.
     *
     * @param strings the strings to place into this list.
     */
    public StringList(String... strings)
    {
        super(strings.length);
        for (String s : strings)
            add(s);
    }

    /**
     * Represents the string list as a comma-separated string, suitable for
     * use as a parameter in various web service API methods.
     */
    @Override
    public String toString()
    {
        if (isEmpty())
            return "";

        StringBuilder sb = new StringBuilder(get(0));
        for (int i=1; i<size(); i++)
            sb.append(',').append(get(i));

        return sb.toString();
    }
}
