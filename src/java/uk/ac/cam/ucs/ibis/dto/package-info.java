/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Provides DTO classes for transferring data from the server to client in
 * the web service API.
 * <p>
 * Most web service API methods return an instance or a list of one of these
 * classes, with all of the requested details in public fields. If an error
 * occurs processing the method on the server, an
 * {@link uk.ac.cam.ucs.ibis.client.IbisException} will be thrown containing
 * an {@link uk.ac.cam.ucs.ibis.dto.IbisError} object with the the details
 * of the error.
 */
@XmlSchema(elementFormDefault = XmlNsForm.QUALIFIED,
           namespace = "http://www.lookup.cam.ac.uk",
           xmlns = { @XmlNs(namespaceURI = "http://www.lookup.cam.ac.uk", prefix = "") })
package uk.ac.cam.ucs.ibis.dto;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
