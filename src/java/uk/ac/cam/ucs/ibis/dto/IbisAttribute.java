/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.dto;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Class representing an attribute of a person or institution returned by
 * the web service API. Note that for institution attributes, the instid,
 * visibility and owningGroupid fields will be {@code null}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class IbisAttribute
{
    /* Format to use for encoding effective dates. */
    private static final DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.UK);

    /** The unique internal identifier of the attribute. */
    @XmlAttribute public Long attrid;

    /** The attribute's scheme. */
    @XmlAttribute public String scheme;

    /** The attribute's value (except for binary attributes). */
    @XmlElement public String value;

    /** The binary data held in the attribute (e.g., a JPEG photo). */
    @XmlElement public byte[] binaryData;

    /** Any comment associated with the attribute. */
    @XmlElement public String comment;

    /**
     * For a person attribute, the optional institution that the attribute
     * is associated with. This will not be set for institution attributes.
     */
    @XmlAttribute public String instid;

    /**
     * For a person attribute, it's visibility ({@code "private"},
     * {@code "institution"}, {@code "university"} or {@code "world"}). This
     * will not be set for institution attributes.
     */
    @XmlAttribute public String visibility;

    /** For time-limited attributes, the date from which it takes effect. */
    @XmlAttribute public Date effectiveFrom;

    /** For time-limited attributes, the date after which it is no longer effective. */
    @XmlAttribute public Date effectiveTo;

    /**
     * For a person attribute, the ID of the group that owns it (typically
     * the user agent group that created it).
     */
    @XmlAttribute public String owningGroupid;

    /* Utility method to base 64 encode a string. */
    private static String base64Encode(String   val)
    {
        try
        {
            return DatatypeConverter.printBase64Binary(val.getBytes("UTF-8"));
        }
        catch (UnsupportedEncodingException e)
        {
            // Should never happen - all sensible JVMs support UTF-8
            throw new RuntimeException(e);
        }
    }

    /**
     * Encode this attribute as an ASCII string suitable for passing as a
     * parameter to a web service API method. This string is compatible with
     * {@link #valueOf(java.lang.String)}.
     * <p>
     * NOTE: This requires that the attribute's {@link #scheme} field be
     * set, and typically the {@link #value} or {@link #binaryData} should
     * also be set.
     *
     * @return The string encoding of this attribute.
     */
    public String encodedString()
    {
        if (scheme == null)
            throw new RuntimeException("Attribute scheme must be set");

        StringBuilder sb = new StringBuilder("scheme:");
        sb.append(base64Encode(scheme));

        if (attrid != null)
            sb.append(",attrid:").append(attrid);
        if (value != null)
            sb.append(",value:").append(base64Encode(value));
        if (binaryData != null)
            sb.append(",binaryData:").append(DatatypeConverter.printBase64Binary(binaryData));
        if (comment != null)
            sb.append(",comment:").append(base64Encode(comment));
        if (instid != null)
            sb.append(",instid:").append(base64Encode(instid));
        if (visibility != null)
            sb.append(",visibility:").append(base64Encode(visibility));
        if (effectiveFrom != null)
            sb.append(",effectiveFrom:").append(dateFormat.format(effectiveFrom));
        if (effectiveTo != null)
            sb.append(",effectiveTo:").append(dateFormat.format(effectiveTo));
        if (owningGroupid != null)
            sb.append(",owningGroupid:").append(base64Encode(owningGroupid));

        return sb.toString();
    }

    /* Utility method to decode a base 64 encoded string. */
    private static String base64Decode(String   val)
    {
        try
        {
            return new String(DatatypeConverter.parseBase64Binary(val), "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            // Should never happen - all sensible JVMs support UTF-8
            throw new RuntimeException(e);
        }
    }

    /**
     * Create an IbisAttribute from its ASCII encoded string representation.
     * This allows an IbisAttribute to be used as a parameter in a web
     * service API method.
     * 
     * @param encodedStr The ASCII string encoding of the attribute, as
     * produced by {@link #encodedString()}.
     * @return The IbisAttribute constructed from the string.
     */
    public static IbisAttribute valueOf(String  encodedStr)
        throws ParseException
    {
        if (encodedStr == null || encodedStr.equals(""))
            return null; /* To support optional API method parameters */

        IbisAttribute attr = new IbisAttribute();
        int idx = 0;

        for (String field : encodedStr.split(","))
        {
            if (field.startsWith("scheme:"))
                attr.scheme = base64Decode(field.substring(7));
            else if (field.startsWith("attrid:"))
                attr.attrid = new Long(field.substring(7));
            else if (field.startsWith("value:"))
                attr.value = base64Decode(field.substring(6));
            else if (field.startsWith("binaryData:"))
                attr.binaryData = DatatypeConverter.parseBase64Binary(field.substring(11));
            else if (field.startsWith("comment:"))
                attr.comment = base64Decode(field.substring(8));
            else if (field.startsWith("instid:"))
                attr.instid = base64Decode(field.substring(7));
            else if (field.startsWith("visibility:"))
                attr.visibility = base64Decode(field.substring(11));
            else if (field.startsWith("effectiveFrom:"))
                attr.effectiveFrom = dateFormat.parse(field.substring(14));
            else if (field.startsWith("effectiveTo:"))
                attr.effectiveTo = dateFormat.parse(field.substring(12));
            else if (field.startsWith("owningGroupid:"))
                attr.owningGroupid = base64Decode(field.substring(14));
            else
                throw new ParseException("Invalid IbisAtttribute encoding - unrecognised field: "+field, idx);

            idx += field.length()+1;
        }
        return attr;
    }
}
