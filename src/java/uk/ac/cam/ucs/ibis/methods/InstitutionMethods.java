/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.ibis.client.ClientConnection;
import uk.ac.cam.ucs.ibis.client.ClientConnection.Method;
import uk.ac.cam.ucs.ibis.client.IbisException;
import uk.ac.cam.ucs.ibis.dto.*;

/**
 * Methods for querying and manipulating institutions.
 *
 * <h4>The fetch parameter for institutions</h4>
 * <p>
 * All methods that return institutions also accept an optional
 * <code>fetch</code> parameter that may be used to request additional
 * information about the institutions returned. For more details about
 * the general rules that apply to the <code>fetch</code> parameter,
 * refer to the {@link PersonMethods} documentation.
 * <p>
 * For institutions the <code>fetch</code> parameter may be used to fetch
 * any institution attribute by specifying the <code>schemeid</code> of an
 * institution attribute scheme. Examples include {@code "address"},
 * {@code "jpegPhoto"}, {@code "universityPhone"}, {@code "instPhone"},
 * {@code "landlinePhone"}, {@code "mobilePhone"}, {@code "faxNumber"},
 * {@code "email"} and {@code "labeledURI"}. The full list (which may be
 * extended over time) may be obtained using {@link #allAttributeSchemes}.
 * <p>
 * In addition the following pseudo-attributes are supported:
 * <ul>
 * <li>{@code "phone_numbers"} - fetches all phone numbers. This is
 * equivalent to
 * {@code "universityPhone,instPhone,landlinePhone,mobilePhone"}.</li>
 * <li>{@code "all_attrs"} - fetches all attributes from all institution
 * attribute schemes. This does not include references.</li>
 * <li>{@code "contact_rows"} - fetches all institution contact rows. Any
 * chained fetches from contact rows are used to fetch attributes from any
 * people referred to by the contact rows.</li>
 * </ul>
 * <p>
 * The <code>fetch</code> parameter may also be used to fetch referenced
 * people, institutions or groups. This will only include references to
 * non-cancelled entities. The following references are supported:
 * <ul>
 * <li>{@code "all_members"} - fetches all the people who are members of the
 * institution.</li>
 * <li>{@code "parent_insts"} - fetches all the parent institutions. Note
 * that currently all institutions have only one parent, but this may change
 * in the future, and client applications should be prepared to handle
 * multiple parents.</li>
 * <li>{@code "child_insts"} - fetches all the child institutions.</li>
 * <li>{@code "inst_groups"} - fetches all the groups that belong to the
 * institution.</li>
 * <li>{@code "members_groups"} - fetches all the groups that form the
 * institution's membership list.</li>
 * <li>{@code "managed_by_groups"} - fetches all the groups that manage the
 * institution's data (commonly called "Editor" groups).</li>
 * </ul>
 * <p>
 * As with person <code>fetch</code> parameters, the references may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced people, institutions or groups. For example
 * {@code "all_members.email"} will fetch the email addresses of all members
 * of the institution. For more information about what can be fetched from
 * referenced people and groups, refer to the documentation for
 * {@link PersonMethods} and {@link GroupMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class InstitutionMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new InstitutionMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public InstitutionMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Return a list of all the institution attribute schemes available.
     * The {@code schemeid} values of these schemes may be used in the
     * <code>fetch</code> parameter of other methods that return institutions.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/all-attr-schemes ]</code>
     *
     * @return All the available institution attribute schemes (in precedence
     * order).
     */
    public java.util.List<IbisAttributeScheme> allAttributeSchemes()
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/all-attr-schemes",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attributeSchemes;
    }

    /**
     * Return a list of all institutions.
     * <p>
     * By default, only a few basic details about each institution are
     * returned, but the optional <code>fetch</code> parameter may be used
     * to fetch additional attributes or references.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/all-insts ]</code>
     *
     * @param includeCancelled [optional] Whether or not to include cancelled
     * institutions. By default, only live institutions are returned.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The requested institutions (in instid order).
     */
    public java.util.List<IbisInstitution> allInsts(boolean includeCancelled,
                                                    String  fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "includeCancelled", includeCancelled,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/all-insts",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.institutions;
    }

    /**
     * Get the institutions with the specified IDs.
     * <p>
     * By default, only a few basic details about each institution are
     * returned, but the optional <code>fetch</code> parameter may be used
     * to fetch additional attributes or references.
     * <p>
     * The results are sorted by ID.
     * <p>
     * NOTE: The URL path length is limited to around 8000 characters, and
     * an instid is up to 8 characters long. Allowing for comma separators
     * and URL encoding, this limits the number of institutions that this
     * method may fetch to around 700.
     * <p>
     * NOTE: The institutions returned may include cancelled institutions.
     * It is the caller's responsibility to check their cancelled flags.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/list?instids=... ]</code>
     *
     * @param instids [required] A comma-separated list of instids.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The requested institutions (in instid order).
     */
    public java.util.List<IbisInstitution> listInsts(String instids,
                                                     String fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "instids", instids,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/list",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.institutions;
    }

    /**
     * Find all institutions modified between the specified pair of
     * transactions.
     * <p>
     * The transaction IDs specified should be the IDs from two different
     * requests for the last (most recent) transaction ID, made at different
     * times, that returned different values, indicating that some Lookup
     * data was modified in the period between the two requests. This method
     * then determines which (if any) institutions were affected.
     * <p>
     * By default, only a few basic details about each institution are
     * returned, but the optional <code>fetch</code> parameter may be used
     * to fetch additional attributes or references.
     * <p>
     * NOTE: All data returned reflects the latest available data about each
     * institution. It is not possible to query for old data, or more
     * detailed information about the specific changes made.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/modified-insts?minTxId=...&maxTxId=... ]</code>
     *
     * @param minTxId [required] Include modifications made in transactions
     * after (but not including) this one.
     * @param maxTxId [required] Include modifications made in transactions
     * up to and including this one.
     * @param instids [optional] Only include institutions with instids in
     * this list. By default, all modified institutions will be included.
     * @param includeCancelled  [optional] Include cancelled institutions. By
     * default, cancelled institutions are excluded.
     * @param contactRowChanges [optional] Include institutions whose contact
     * rows have changed. By default, changes to institution contact rows are
     * not taken into consideration.
     * @param membershipChanges [optional] Include institutions whose members
     * have changed. By default, changes to institutional memberships are not
     * taken into consideration.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The modified institutions (in instid order).
     */
    public java.util.List<IbisInstitution> modifiedInsts(long       minTxId,
                                                         long       maxTxId,
                                                         String     instids,
                                                         boolean    includeCancelled,
                                                         boolean    contactRowChanges,
                                                         boolean    membershipChanges,
                                                         String     fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "minTxId", minTxId,
                                 "maxTxId", maxTxId,
                                 "instids", instids,
                                 "includeCancelled", includeCancelled,
                                 "contactRowChanges", contactRowChanges,
                                 "membershipChanges", membershipChanges,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/modified-insts",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.institutions;
    }

    /**
     * Search for institutions using a free text query string. This is the
     * same search function that is used in the Lookup web application.
     * <p>
     * By default, only a few basic details about each institution are
     * returned, but the optional <code>fetch</code> parameter may be used
     * to fetch additional attributes or references.
     * <p>
     * NOTE: If the query string starts with the prefix {@code "inst:"}, it
     * is treated as an <a href="/lql" target="_top">LQL query</a>, allowing
     * more advanced searches. An LQL query will ignore the
     * <code>approxMatches</code> and <code>attributes</code> parameters, but
     * it will respect the value of <code>includeCancelled</code>. In
     * addition, an LQL query will ignore the <code>orderBy</code> parameter,
     * since LQL queries always return results in ID order.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/search?query=... ]</code>
     *
     * @param query [required] The search string.
     * @param approxMatches [optional] Flag to enable more approximate
     * matching in the search, causing more results to be returned. Defaults
     * to {@code false}. This is ignored for LQL queries.
     * @param includeCancelled [optional] Flag to allow cancelled institutions
     * to be included. Defaults to {@code false}.
     * @param attributes [optional] A comma-separated list of attributes to
     * consider when searching. If this is {@code null} (the default) then
     * all attribute schemes marked as searchable will be included. This is
     * ignored for LQL queries.
     * @param offset [optional] The number of results to skip at the start
     * of the search. Defaults to 0.
     * @param limit [optional] The maximum number of results to return.
     * Defaults to 100.
     * @param orderBy [optional] The order in which to list the results.
     * This may be either {@code "instid"} or {@code "name"} (the default for
     * non-LQL queries). This is ignored for LQL queries, which always return
     * results in instid order.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The matching institutions.
     */
    public java.util.List<IbisInstitution> search(String    query,
                                                  boolean   approxMatches,
                                                  boolean   includeCancelled,
                                                  String    attributes,
                                                  int       offset,
                                                  int       limit,
                                                  String    orderBy,
                                                  String    fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "query", query,
                                 "approxMatches", approxMatches,
                                 "includeCancelled", includeCancelled,
                                 "attributes", attributes,
                                 "offset", offset,
                                 "limit", limit,
                                 "orderBy", orderBy,
                                 "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/search",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.institutions;
    }

    /**
     * Count the number of institutions that would be returned by a search
     * using a free text query string.
     * <p>
     * NOTE: If the query string starts with the prefix {@code "inst:"}, it
     * is treated as an <a href="/lql" target="_top">LQL query</a>, allowing
     * more advanced searches. An LQL query will ignore the
     * <code>approxMatches</code> and <code>attributes</code> parameters, but
     * it will respect the value of <code>includeCancelled</code>.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/search-count?query=... ]</code>
     *
     * @param query [required] The search string.
     * @param approxMatches [optional] Flag to enable more approximate
     * matching in the search, causing more results to be returned. Defaults
     * to {@code false}. This is ignored for LQL queries.
     * @param includeCancelled [optional] Flag to allow cancelled institutions
     * to be included. Defaults to {@code false}.
     * @param attributes [optional] A comma-separated list of attributes to
     * consider when searching. If this is {@code null} (the default) then
     * all attribute schemes marked as searchable will be included. This is
     * ignored for LQL queries.
     *
     * @return The number of matching institutions.
     */
    public int searchCount(String   query,
                           boolean  approxMatches,
                           boolean  includeCancelled,
                           String   attributes)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "query", query,
                                 "approxMatches", approxMatches,
                                 "includeCancelled", includeCancelled,
                                 "attributes", attributes };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/search-count",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return Integer.parseInt(result.value);
    }

    /**
     * Get the institution with the specified ID.
     * <p>
     * By default, only a few basic details about the institution are
     * returned, but the optional <code>fetch</code> parameter may be used
     * to fetch additional attributes or references of the institution.
     * <p>
     * NOTE: The institution returned may be a cancelled institution. It is
     * the caller's responsibility to check its cancelled flag.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/{instid} ]</code>
     *
     * @param instid [required] The ID of the institution to fetch.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch.
     *
     * @return The requested institution or {@code null} if it was not found.
     */
    public IbisInstitution getInst(String   instid,
                                   String   fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.institution;
    }

    /**
     * Add an attribute to an institution. By default, this will not add the
     * attribute again if it already exists.
     * <p>
     * When adding an attribute, the new attribute's scheme must be set.
     * In addition, either its value or its binaryData field should be set.
     * All the remaining fields of the attribute are optional.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: POST /api/v1/inst/{instid}/add-attribute ]</code>
     *
     * @param instid [required] The ID of the institution.
     * @param attr [required] The new attribute to add.
     * @param position [optional] The position of the new attribute in the
     * list of attributes of the same attribute scheme (1, 2, 3,...). A value
     * of 0 (the default) will cause the new attribute to be added to the end
     * of the list of existing attributes for the scheme.
     * @param allowDuplicates [optional] If {@code true}, the new attribute
     * will always be added, even if another identical attribute already
     * exists. If {@code false} (the default), the new attribute will only be
     * added if it doesn't already exist.
     * @param commitComment [recommended] A short textual description of
     * the change made (will be visible on the history tab in the web
     * application).
     *
     * @return The newly created or existing attribute.
     */
    public IbisAttribute addAttribute(String        instid,
                                      IbisAttribute attr,
                                      int           position,
                                      boolean       allowDuplicates,
                                      String        commitComment)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid };
        Object[] queryParams = {  };
        Object[] formParams = { "attr", attr,
                                "position", position,
                                "allowDuplicates", allowDuplicates,
                                "commitComment", commitComment };
        IbisResult result = conn.invokeMethod(Method.POST,
                                              "api/v1/inst/%1$s/add-attribute",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attribute;
    }

    /**
     * Get all the cancelled members of the specified institution.
     * <p>
     * By default, only a few basic details about each member are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of each person.
     * <p>
     * NOTE: This method returns only cancelled people. It does not include
     * people who were removed from the institution. Cancelled people are no
     * longer considered to be current staff, students or accredited visitors,
     * and are no longer regarded as belonging to any groups or institutions.
     * The list returned here reflects their institutional memberships just
     * before they were cancelled, and so is out-of-date data that should be
     * used with caution.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/{instid}/cancelled-members ]</code>
     *
     * @param instid [required] The ID of the institution.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch for each person.
     *
     * @return The institution's cancelled members (in identifier order).
     */
    public java.util.List<IbisPerson> getCancelledMembers(String    instid,
                                                          String    fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/%1$s/cancelled-members",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Get all the contact rows of the specified institution.
     * <p>
     * Any addresses, email addresses, phone numbers and web pages
     * associated with the contact rows are automatically returned, as
     * well as any people referred to by the contact rows.
     * <p>
     * If any of the contact rows refer to people, then only a few basic
     * details about each person are returned, but the optional
     * <code>fetch</code> parameter may be used to fetch additional
     * attributes or references of each person.
     * <p>
     * NOTE: This method will not include cancelled people.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/{instid}/contact-rows ]</code>
     *
     * @param instid [required] The ID of the institution.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch for any people referred to by any
     * of the contact rows.
     *
     * @return The institution's contact rows.
     */
    public java.util.List<IbisContactRow> getContactRows(String instid,
                                                         String fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/%1$s/contact-rows",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.institution.contactRows;
    }

    /**
     * Create a group owned by an institution.
     * <p>
     * A new group will be created with the institution being its only owner.
     * <p>
     * Only limited attributes (short hyphenated name, title and description) of
     * the new group can be specified when creating a group. The GroupMethod's
     * update methods be used to modify other attributes.
     * The new group will have membership visibility of 'university' and be managed
     * by the same groups as its owning institution.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: POST /api/v1/inst/{instid}/create-group ]</code>
     *
     * @param instid [required] The ID of the institution.
     * @param name [required] A short hyphenated name for the new group. Must be in lower case and
     * begin with the instid followed by a hyphen.
     * @param title [required] A title for the new group.
     * @param description [required] A more detailed description of the new group.
     * @param managedBy [optional] The sole group that will manage group data for the new group.
     * If not provided, the new group will be managed by the same groups as its owning institution.
     * @param commitComment [recommended] A short textual description of
     * the change made (will be visible on the history tab in the web
     * application).
     *
     * @return The newly created group.
     */
    public IbisGroup createGroup(String instid,
                                 String name,
                                 String title,
                                 String description,
                                 String managedBy,
                                 String commitComment)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid };
        Object[] queryParams = {  };
        Object[] formParams = { "name", name,
                                "title", title,
                                "description", description,
                                "managedBy", managedBy,
                                "commitComment", commitComment };
        IbisResult result = conn.invokeMethod(Method.POST,
                                              "api/v1/inst/%1$s/create-group",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.group;
    }

    /**
     * Get one or more (possibly multi-valued) attributes of an institution.
     * The returned attributes are sorted by attribute scheme precedence and
     * then attribute precedence.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/{instid}/get-attributes?attrs=... ]</code>
     *
     * @param instid [required] The ID of the institution.
     * @param attrs [required] The attribute scheme(s) to fetch. This may
     * include any number of the attributes or pseudo-attributes, but it
     * may not include references or attribute chains (see the documentation
     * for the {@code fetch} parameter in this class).
     *
     * @return The requested attributes.
     */
    public java.util.List<IbisAttribute> getAttributes(String   instid,
                                                       String   attrs)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid };
        Object[] queryParams = { "attrs", attrs };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/%1$s/get-attributes",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attributes;
    }

    /**
     * Get all the members of the specified institution.
     * <p>
     * By default, only a few basic details about each member are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of each person.
     * <p>
     * NOTE: This method will not include cancelled people.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/{instid}/members ]</code>
     *
     * @param instid [required] The ID of the institution.
     * @param fetch [optional] A comma-separated list of any additional
     * attributes or references to fetch for each person.
     *
     * @return The institution's members (in identifier order).
     */
    public java.util.List<IbisPerson> getMembers(String instid,
                                                 String fetch)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/%1$s/members",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.people;
    }

    /**
     * Delete an attribute of an institution. It is not an error if the
     * attribute does not exist.
     * <p>
     * Note that in this method, the <code>commitComment</code> is passed
     * as a query parameter, rather than as a form parameter, for greater
     * client compatibility.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: DELETE /api/v1/inst/{instid}/{attrid} ]</code>
     *
     * @param instid [required] The ID of the institution.
     * @param attrid [required] The ID of the attribute to delete.
     * @param commitComment [recommended] A short textual description of
     * the change made (will be visible on the history tab in the web
     * application).
     *
     * @return {@code true} if the attribute was deleted by this method, or
     * {@code false} if it did not exist.
     */
    public boolean deleteAttribute(String   instid,
                                   Long     attrid,
                                   String   commitComment)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid, ""+attrid };
        Object[] queryParams = { "commitComment", commitComment };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.DELETE,
                                              "api/v1/inst/%1$s/%2$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return Boolean.parseBoolean(result.value);
    }

    /**
     * Get a specific attribute of an institution.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/inst/{instid}/{attrid} ]</code>
     *
     * @param instid [required] The ID of the institution.
     * @param attrid [required] The ID of the attribute to fetch.
     *
     * @return The requested attribute.
     */
    public IbisAttribute getAttribute(String    instid,
                                      Long      attrid)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid, ""+attrid };
        Object[] queryParams = {  };
        Object[] formParams = {  };
        IbisResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/inst/%1$s/%2$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attribute;
    }

    /**
     * Update an attribute of an institution.
     * <p>
     * The attribute's value, binaryData, comment and effective date fields
     * will all be updated using the data supplied. All other fields will be
     * left unchanged.
     * <p>
     * To avoid inadvertently changing fields of the attribute, it is
     * recommended that {@link #getAttribute getAttribute()} be used to
     * retrieve the current value of the attribute, before calling this
     * method with the required changes.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: PUT /api/v1/inst/{instid}/{attrid} ]</code>
     *
     * @param instid [required] The ID of the institution.
     * @param attrid [required] The ID of the attribute to update.
     * @param attr [required] The new attribute values to apply.
     * @param commitComment [recommended] A short textual description of
     * the change made (will be visible on the history tab in the web
     * application).
     *
     * @return The updated attribute.
     */
    public IbisAttribute updateAttribute(String         instid,
                                         Long           attrid,
                                         IbisAttribute  attr,
                                         String         commitComment)
        throws IbisException, IOException, JAXBException
    {
        String[] pathParams = { instid, ""+attrid };
        Object[] queryParams = {  };
        Object[] formParams = { "attr", attr,
                                "commitComment", commitComment };
        IbisResult result = conn.invokeMethod(Method.PUT,
                                              "api/v1/inst/%1$s/%2$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new IbisException(result.error);
        return result.attribute;
    }
}
