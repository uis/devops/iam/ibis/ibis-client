/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.ibis.dto.IbisResult.EntityMap;

/**
 * Class representing a person returned by the web service API. Note that
 * the identifier is the person's primary identifier (typically their CRSid),
 * regardless of which identifier was used to query for the person.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class IbisPerson
{
    /** Flag indicating if the person is cancelled. */
    @XmlAttribute public Boolean cancelled;

    /** The person's primary identifier (typically their CRSid). */
    @XmlElement public IbisIdentifier identifier;

    /** The person's display name (if visible). */
    @XmlElement public String displayName;

    /** The person's registered name (if visible). */
    @XmlElement public String registeredName;

    /** The person's surname (if visible). */
    @XmlElement public String surname;

    /**
     * The person's display name if that is visible, otherwise their
     * registered name if that is visible, otherwise their surname if
     * that is visible, otherwise the value of their primary identifier
     * (typically their CRSid) which is always visible.
     */
    @XmlElement public String visibleName;

    /**
     * The person's MIS status ({@code "staff"}, {@code "student"},
     * {@code "staff,student"} or {@code ""}).
     */
    @XmlElement public String misAffiliation;

    /**
     * A list of the person's identifiers. This will only be populated if
     * the <code>fetch</code> parameter included the
     * {@code "all_identifiers"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "identifier")
    public List<IbisIdentifier> identifiers;

    /**
     * A list of the person's attributes. This will only be populated if the
     * <code>fetch</code> parameter includes the {@code "all_attrs"} option,
     * or any specific attribute schemes such as {@code "email"} or
     * {@code "title"}, or the special pseudo-attribute scheme
     * {@code "phone_numbers"}.
     */
    @XmlElementWrapper
    @XmlElement(name = "attribute")
    public List<IbisAttribute> attributes;

    /**
     * A list of all the institution's to which the person belongs. This
     * will only be populated if the <code>fetch</code> parameter includes
     * the {@code "all_insts"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "institution")
    public List<IbisInstitution> institutions;

    /**
     * A list of all the groups to which the person belongs, including
     * indirect group memberships, via groups that include other groups.
     * This will only be populated if the <code>fetch</code> parameter
     * includes the {@code "all_groups"} option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> groups;

    /**
     * A list of all the groups that the person directly belongs to. This
     * does not include indirect group memberships - i.e., groups that
     * include these groups. This will only be populated if the
     * <code>fetch</code> parameter includes the {@code "direct_groups"}
     * option.
     */
    @XmlElementWrapper
    @XmlElement(name = "group")
    public List<IbisGroup> directGroups;

    /* == Some utility methods == */

    /**
     * Returns {@code true} if the person is a member of staff.
     * <p>
     * Note that this tests for an misAffiliation of {@code ""},
     * {@code "staff"} or {@code "staff,student"} since some members of staff
     * will have a blank misAffiliation.
     */
    public boolean isStaff()
    {
        return misAffiliation == null || !misAffiliation.equals("student");
    }

    /**
     * Returns {@code true} if the person is a student.
     * <p>
     * This tests for an misAffiliation of {@code "student"} or
     * {@code "staff,student"}.
     */
    public boolean isStudent()
    {
        return misAffiliation != null && misAffiliation.contains("student");
    }

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this person within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to a person element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "person/"+identifier; }

    /** Return a new IbisPerson that refers to this person. */
    public IbisPerson ref() { IbisPerson p = new IbisPerson(); p.ref = id; return p; }

    /* == Code to help clients unflatten an IbisResult object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single IbisPerson. */
    protected IbisPerson unflatten(EntityMap    em)
    {
        if (ref != null)
        {
            IbisPerson person = em.getPerson(ref);
            if (!person.unflattened)
            {
                person.unflattened = true;
                IbisInstitution.unflatten(em, person.institutions);
                IbisGroup.unflatten(em, person.groups);
                IbisGroup.unflatten(em, person.directGroups);
            }
            return person;
        }
        return this;
    }

    /** Unflatten a list of IbisPerson objects (done in place). */
    protected static void unflatten(EntityMap           em,
                                    List<IbisPerson>    people)
    {
        if (people != null)
            for (int i=0; i<people.size(); i++)
                people.set(i, people.get(i).unflatten(em));

    }
}
