/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.client;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * Null hostname verifier for checking hostnames in certificates. This
 * disables all checking, and just trusts the server.
 * <p>
 * This should only be used for testing, when connecting to a server that
 * doesn't have valid certificates.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
/* package */ class NullHostnameVerifier implements HostnameVerifier
{
    @Override
    public boolean verify(String hostname, SSLSession session)
    {
        return true; // Allow all hosts
    }
}
