/*
Copyright (c) 2012, University of Cambridge Computing Service

This file is part of the Lookup/Ibis client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.ibis.client;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/**
 * Null trust manager for checking X509 certificates. This disables all
 * checking, and just trusts the server.
 * <p>
 * This should only be used for testing, when connecting to a server that is
 * using self-signed certificates.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
/* package */ class NullTrustManager implements X509TrustManager
{
    @Override
    public void checkClientTrusted(X509Certificate[]    chain,
                                   String               authType)
        throws CertificateException
    {
        // Do nothing (trust the client unconditionally)
    }

    @Override
    public void checkServerTrusted(X509Certificate[]    chain,
                                   String               authType)
        throws CertificateException
    {
        // Do nothing (trust the server unconditionally)
    }

    @Override
    public X509Certificate[] getAcceptedIssuers()
    {
        return null; // No CA certificates
    }
}
