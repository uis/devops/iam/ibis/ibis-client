#!/bin/sh
# Build the deliverables.

cleanup() {
  exit_code=$?
  UID_GID="$(id -u):$(id -g)" docker-compose --project-name ibis-client down
  exit $exit_code
}
trap cleanup 0 1 2 3 6

UID_GID="$(id -u):$(id -g)" docker-compose --project-name ibis-client up --build --abort-on-container-exit build ${@}
