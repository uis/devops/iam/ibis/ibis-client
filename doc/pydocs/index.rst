.. Lookup/Ibis web service API documentation

Lookup/Ibis web service client API
==================================

Modules:

.. toctree::
   :maxdepth: 1

   ibisclient.connection
   ibisclient.dto
   ibisclient.methods

This document describes the Lookup/Ibis web service client API, which
is intended to simplify the task of using the web service from Python
client applications.

This client library provides code to automatically handle the
following tasks:

* Securely connect to the server over HTTPS, checking its certificates
  to ensure that it is really is the Lookup/Ibis server.

* Authenticate with the server using HTTP Basic Authentication.

* Construct correct URLs to methods on the server, encoding any method
  parameters that may form part of the path, or appear in the query
  string.

* Encode any form parameters that may be needed in the request body.

* Parse the XML returned by the server, producing one or more linked
  Python objects containing all the requested information in an easily
  accessible form.

All of the API methods are contained in `XxxMethods`
classes in the :any:`methods` module, for example
:any:`PersonMethods`
which contains methods relating to or returning people, and
:any:`InstitutionMethods`
which contains methods relating to or returning institutions. All
the `XxxMethods` classes are auto-generated, to keep
them in sync with the matching server methods.

Some typical code using this API might look something like this:

.. code-block:: python

    from ibisclient import *

    # Create an IbisClientConnection object
    conn = createTestConnection()

    # Create an InstitutionMethods object
    im = InstitutionMethods(conn)

    # Fetch the members of the UIS institution
    people = im.getMembers("UIS")

    # Print the results
    for person in people:
        print(person.visibleName)

Note that it is typically only necessary to create a single instance
of the :any:`IbisClientConnection` and `XxxMethods`
objects, and then re-use them throughout your application.

**Thread safety**

Once created and initialised, the :any:`IbisClientConnection` and
`XxxMethods` classes are safe to use from multiple
simultaneous threads (for example in a web server).

Note, however, that
:any:`IbisClientConnection.set_username()` and
:any:`IbisClientConnection.set_password()` are not thread safe. These methods
are regarded as part of the initialisation of the connection, and
should typically only be used once on startup. If you need to
regularly switch between users, then use a separate
:any:`IbisClientConnection` per user or per thread (and separate
corresponding `XxxMethods` objects).
